#!/usr/bin/env python3
import xml2dict
import json

fname = r"/home/coder/aktueller-kurs/materialien/books.xml"

print(json.dumps(xml2dict.parse(open(fname).read()), indent=4))