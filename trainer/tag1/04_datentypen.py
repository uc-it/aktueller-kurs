#!/usr/bin/env python3
var = 4711
print(type(var), var)
var = "Hello"
print(type(var), var)
var = 12.34
print(type(var), var)
var = None
print(type(var), var)
var = [1,2,3]
print(type(var), var)
var = (1,2,3)
print(type(var), var)
var = {"a":1, "b":2, "c":3}
print(type(var), var)
var = {1, 2, 3}
print(type(var), var)