#!/usr/bin/env python3
eingabe = input("Eine Ganzzahl: ")

if eingabe.isdigit():
    print(f"Es ist eine Ganzzahl {int(eingabe)}")
else:
    print(f"Ungültige Eingabe {eingabe}")