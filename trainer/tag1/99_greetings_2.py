print("Hallo Besucher!")
name = input("Wie ist Ihr Name? ")

# Schön + richtig
if name == "Willi" or name == "Heinz":
    # True-Zweig
    print("Hallo alter Freund")
else:
    # False-Zweig
    print(f"Hallo {name}")

# Hässlich + richtig
if name == "Willi":
    # True-Zweig
    print("Hallo alter Freund")
elif name == "Heinz":
    print("Hallo alter Freund")
else:
    # False-Zweig
    print(f"Hallo {name}")

# python-Art, elegant + richtig
if name in ["Willi", "Heinz"]:
    # True-Zweig
    print("Hallo alter Freund")
else:
    # False-Zweig
    print(f"Hallo {name}")