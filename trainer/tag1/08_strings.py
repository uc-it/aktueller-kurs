#!/usr/bin/env python3
import string

s = "Hello, world"
print(list(s))
print(len(s))

print(s[0])

print(s[11])
print(s[len(s)-1])
print(s[-1])

print(s[:5])
print(s[7:])

print(s[2:5])

print(s[:])
print(s[::2])

print(string.ascii_lowercase) # Konstante der Klasse string, die alle gültigen Kleinbuchstaben enthält
print(s.lower()) # Methode der Klasse string, gebunden an Objekt s, um den Inhlat klein zurück zu geben

if s.isdecimal():
    print("ist eine Ganzzahl")
elif s.isalpha():
    print("ist nur Alphanumerisch")
elif s.isprintable():
    print("immerhin druckbar")
else:
    print("dann eben sonst was!")
