def frange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

for n in frange(0, 5, 0.1):
    print(type(n), n, sep=" ")