#!/usr/bin/env python3

d = {}
try:
    print(d["Name"])
except KeyError:
    print("Nix")

print(d.get("Name", "Nix"))

#-------------------------

x = ('key1', 'key2', 'key3')
y = 0

thisdict = dict.fromkeys(x, y)

print(thisdict)

print("-" * 80)
car = {

  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}

car.update({"color": "White"})
car.update({"brand": "Ford Car Company"})

print(car)