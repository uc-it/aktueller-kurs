#!/usr/bin/env python3
l = [1, 2, 3]
t = (1, 2, 3)

print(t == l)

l[0] = 4711
# t[0] = 4711 gitb Exception

print(l[0])
print(t[0])

l.append(4711)
# t.append(4711)

l = list(t) # legt kopie des tupels als liste an
l[0] = 4711
print(t[0])
print(l[0])