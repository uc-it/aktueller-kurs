#!/usr/bin/env python3
def funktion_1(a):
    assert isinstance(a, str), 'Argument of wrong type!' 

def funktion_2(a):
    if not isinstance(a, str):
        raise TypeError("Invalid Type")  

def funktion_3(a):
    v = str(a)

#funktion_1(4711)
#funktion_2(4711)
funktion_3(4711)