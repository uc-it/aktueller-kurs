#!/usr/bin/env python3
s = "Willi:Watz:Watzplatz:17:12345:Watzingen"
l = s.split(":") 
print(l)

n = "|".join(l)
print(n)

# Gezielt variablen aus listen heraus füllen
# _ ist die default/dummy Variable
name, _, _, _, _, ort = l
print(name, "aus",  ort)

name, vorname, *adresse = l
# oder gleich: name, vorname, adresse = s.split(":") 
print(name)
print(vorname)
print(adresse)

print("-" * 80)
a = 1
b = 100
# Aufgabe: tausche a mit b
b, a = a, b
print("a:", a)
print("b:", b)

print("-" * 80)
for n in ["Watzingen", "12345", "Am Platz", "10"]:
    if n in adresse:
        print(n, "identisch")
    else:
        print(n, "nicht gefunden")

    