#!/usr/bin/env python3
alter   = float(input("Wie alt bist Du? "))
groesse = float(input("Wie groß bist Du? (in m) "))
gewicht = float(input("Wie schwer bist Du? (in kg) "))
bmi = gewicht / (groesse ** 2)

if alter < 19:
    un = 0
elif alter < 25:
    un = 19
    ob = 25
elif alter < 35:
    un = 20
    ob = 26
elif alter < 45:
    un = 21
    ob = 27
elif alter < 55:
    un = 22
    ob = 28
elif alter < 65:
    un = 23
    ob = 29
else:
    un = 24
    ob = 30

print(f"Dein BMI ist {bmi}")

if un == 0:
    print("Werd erstmal alt genug.")
elif bmi < un:
    print("Du bist ein Hungerhaken. Hau rein!!!")
elif bmi < ob:
    print("Guten Appetit.")
else:
    print("Jetzt aber mal langsam, geh Dich mal wiegen!")
    