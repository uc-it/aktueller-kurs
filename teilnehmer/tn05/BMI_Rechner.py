#!usr/bin/env python3
alterkg = input("Bitte Ihr Alter eingeben: ")
groesse = input("Bitte Ihre Größe in cm eingeben: ")
gewicht = input("Bitte Ihr Gewicht in kg eingeben: ")

bmi = float(gewicht) / ((float(groesse)/100)**2)

if bmi < 20:
    print(f"BMI von: {bmi}, ---> Starkes Untergewicht")
elif bmi < 25:
    print(f"BMI von: {bmi}, ---> Normales Untergewicht")
elif bmi < 30:
    print(f"BMI von: {bmi}, ---> Übergewicht")
elif bmi < 35:
    print(f"BMI von: {bmi}, ---> Starkes Übergewicht")
elif bmi <40:
    print(f"BMI von: {bmi}, ---> Adipositas Grad II")
elif bmi >= 40:
    print(f"BMI von: {bmi}, ---> Adipositas Grad III")


