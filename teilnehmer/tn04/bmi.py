print ("Willkommen lieber User zur Abfrage ihrer Daten:")

#geschlecht = int(input ("Gib 1 für weiblich und 2 für männlich ein!  "))
alter = int(input ("Gib dein Alter in Jahren am:  "))
gewicht = float(input("Gib dein Gewicht in kg an: "))
groesse = float(input("Gib deine Größe in m ein: "))

if alter in range(19,25):
        u = 19
        o = 24
elif alter in range(25,35):
        u = 20
        o = 25
elif alter in range(35,45):
        u = 21
        o = 26
elif alter in range(45, 55):
        u = 22
        o = 27
elif alter in range(55,66):
        u = 23
        o = 28
elif alter > 65:
        u = 24
        o = 29
else:
    print("Ihr alter liegt unter 19 Jahren.")
    exit(1)

BMI = gewicht / (groesse * groesse)
print("Dein BMI beträgt " + str(BMI))

if BMI < u:
    print("D.h. du bist untergewichtig.")
elif BMI > o:
    print("D.h. du hast Übergewicht.")
else:
    print("Das entspricht Normalgewicht.")