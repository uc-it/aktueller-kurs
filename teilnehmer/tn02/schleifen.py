#!/usr/bin/env python3

l=[1,2.34,"Willi",None,True]
print("Typ",(type(l)))

print("-"*80)

for e in l:
    print ("Type: ",e,type(e))

print("-"*80)


from random import randrange
i=1
for i in range(0,10000):
    num=randrange(1000)
    print(f"{i}: {num}")
